def reverse_words(text: str) -> str:
    text_list = text.split(' ')
    reversed_text_list = []
    for word in text_list:
        word = list(word)
        i = 0
        j = len(word) - 1
        while i < j:
            if not word[i].isalpha():
                i += 1
            elif not word[j].isalpha():
                j -= 1
            else:
                word[i], word[j] = word[j], word[i]
                i += 1
                j -= 1
        word = ''.join(word)
        reversed_text_list.append(word)

    reversed_text = ' '.join(reversed_text_list)
    return reversed_text


if __name__ == '__main__':
    print(reverse_words('a1bcd efg!h 35fgfy gytuy'))
    cases = [
        ('abcd efgh', 'dcba hgfe'),
        ('a1bcd efg!h', 'd1cba hgf!e'),
        ('', ''),
    ]

    for text, reversed_text in cases:
        assert reverse_words(text) == reversed_text
